# Основное:
**Events**(CTEvents) - **_серверный_** модуль-связующее между CTweaker и остальными модулями.

**Обязателен при установке ClientTweaker.**

# Зависимости:
* ClientTweaker[2.0.0,)
* Java-WebSocket https://gitlab.com/ctmodules/events/-/blob/master/libs/Java-WebSocket-1.4.0-with-dependencies.jar

# Установка:
* Скачать мод с репозитория
* Закинуть мод в папку *mods* на стороне **_сервера_**