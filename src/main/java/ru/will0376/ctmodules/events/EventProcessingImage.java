package ru.will0376.ctmodules.events;

import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

import java.awt.image.BufferedImage;

@Cancelable
public class EventProcessingImage extends Event {
	BufferedImage image;
	String admin;
	String player;
	boolean wbs;
	boolean report;

	public EventProcessingImage(BufferedImage image, String admin, String player, boolean wbs, boolean report) {
		this.image = image;
		this.admin = admin;
		this.player = player;
		this.wbs = wbs;
		this.report = report;
	}

	public BufferedImage getImage() {
		return image;
	}

	public String getAdmin() {
		return admin;
	}

	public String getPlayer() {
		return player;
	}

	public boolean isWbs() {
		return wbs;
	}

	public boolean isReport() {
		return report;
	}
}
