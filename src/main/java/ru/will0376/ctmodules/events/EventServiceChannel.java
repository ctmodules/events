package ru.will0376.ctmodules.events;

import net.minecraftforge.fml.common.eventhandler.Event;

public class EventServiceChannel extends Event {
	private final String act;
	private final String from;
	private final String text;

	public EventServiceChannel(String act, String from, String text) {
		this.act = act;
		this.from = from;
		this.text = text;
	}

	public String getAct() {
		return act;
	}

	public String getFrom() {
		return from;
	}

	public String getText() {
		return text;
	}
}
