package ru.will0376.ctmodules.events;

import net.minecraftforge.fml.common.eventhandler.Event;

public class EventLogPrint extends Event {
	private final int mode;
	private final String domian;
	private final String text;

	public EventLogPrint(int mode, String domian, String text) {
		this.mode = mode;
		this.domian = domian;
		this.text = text;
	}

	public int getMode() {
		return mode;
	}

	public String getDomian() {
		return domian;
	}

	public String getText() {
		return text;
	}
}
