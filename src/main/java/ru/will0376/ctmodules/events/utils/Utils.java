package ru.will0376.ctmodules.events.utils;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import ru.will0376.ctmodules.events.EventLogPrint;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Utils {
	public static boolean checkPermission(ICommandSender sender, String permkey) {
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, "[CheckPermission]", "Check perm: " + permkey + " for sender: " + sender.getName()));
		if (sender.canUseCommand(4, "ct.allperms") || sender.canUseCommand(4, permkey) || sender.getName().equals("Server"))
			return true;
		else {
			sender.sendMessage(new TextComponentString(ChatForm.prefix_error + TextFormatting.RED + "You do not have the right to command!"));
			return false;
		}
	}

	public static EntityPlayerMP getEPMP(String nick) {
		for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers())
			if (player.getName().equals(nick))
				return player;
		return null;
	}

	/*
	*
		public static String usage = Ctreport.MOD_ID +": {\n" +
			" /"+Ctreport.config.getMainCommand() + " <player>\n" +
			"\n" +
			" Aliases: ["+String.join(", ", Ctreport.config.getAliases())+"]\n" +
			" Permissions: [\n" +
			"   ctmodules.report.admin.cooldownbypass\n" +
			"   ctmodules.report.admin.reportwlbypass\n" +
			"   ctmodules.report.admin.blacklist\n" +
			"  ]\n" +
			"}";
	* */
	public static String makeUsage(String modid, String usage, String aliases, String permissions) {
		return modid + ": {\n" +
				usage +
				"\n" +
				aliases +
				"\n" +
				permissions +
				"\n}"
				;
	}

	private BufferedImage convertStringToBufferedImage(String image) {
		byte[] imageInByte = image.getBytes();

		String ii = image;
		InputStream in = new ByteArrayInputStream(imageInByte);
		BufferedImage bImageFromConvert = null;
		try {

			bImageFromConvert = ImageIO.read(in);

			return bImageFromConvert;
		} catch (IOException e) {
		}
		return null;
	}
}
