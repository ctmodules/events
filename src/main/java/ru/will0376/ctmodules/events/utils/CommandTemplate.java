package ru.will0376.ctmodules.events.utils;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommandTemplate extends CommandBase {
	String usage = "";
	String aliases = "";
	String permissions = "";

	public String getName() {
		return "";
	}

	public String getUsage(ICommandSender sender) {
		return "";
	}

	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {

	}

	public List getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
		if (args.length == 1) return getListOfStringsMatchingLastWord(args, "");
		else
			return args.length >= 2 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) : Collections.emptyList();
	}

	public List<String> getAliases() {
		ArrayList<String> al = new ArrayList<>();
		return al;
	}


}
