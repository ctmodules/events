package ru.will0376.ctmodules.events.utils;

import net.minecraft.util.text.TextFormatting;

public class ChatForm {
	public static final String prefix = TextFormatting.GOLD + "[ClientTweaker]: " + TextFormatting.RESET;
	public static final String report_prefix = TextFormatting.GOLD + "[Report]: " + TextFormatting.RESET;
	public static final String prefix_error = TextFormatting.RED + "[ClientTweaker_error]: " + TextFormatting.RESET;
	public static final String report_prefix_error = TextFormatting.RED + "[Report_error]: " + TextFormatting.RESET;
	public static final String prefix_error_whitelist = TextFormatting.RED + "[ClientTweaker_error_whitelist]: " + TextFormatting.RESET;
}

