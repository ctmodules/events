package ru.will0376.ctmodules.events;

import net.minecraftforge.fml.common.eventhandler.Event;

public class EventPrintModList extends Event {
	private final String adminNick;
	private final String text;
	private final String playerNick;

	public EventPrintModList(String adminNick, String text, String playerNick) {
		this.adminNick = adminNick;
		this.text = text;
		this.playerNick = playerNick;
	}

	public String getAdminNick() {
		return adminNick;
	}

	public String getText() {
		return text;
	}

	public String getPlayerNick() {
		return playerNick;
	}
}
