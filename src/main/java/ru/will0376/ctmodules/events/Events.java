package ru.will0376.ctmodules.events;

import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(
		modid = Events.MOD_ID,
		name = Events.MOD_NAME,
		version = Events.VERSION,
		serverSideOnly = true,
		acceptableRemoteVersions = "*",
		dependencies = "required-before:clienttweak@[2.0.0,)"
)
public class Events {

	public static final String MOD_ID = "events";
	public static final String MOD_NAME = "Events";
	public static final String VERSION = "1.0.2.3";

	@Mod.Instance(MOD_ID)
	public static Events INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {

	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, TextFormatting.GOLD + "[Events]" + TextFormatting.RESET, "PostInit done!"));
		MinecraftForge.EVENT_BUS.post(new EventModStatusResponse(MOD_ID, true));
	}
}
