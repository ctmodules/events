package ru.will0376.ctmodules.events;

import net.minecraftforge.fml.common.eventhandler.Event;

public class EventRequestScreen extends Event {
	private final String adminNick;
	private final String playerNick;
	private final boolean wbs;
	private final boolean report;

	public EventRequestScreen(String adminNick, String playerNick, boolean wbs, boolean report) {
		this.adminNick = adminNick;
		this.playerNick = playerNick;
		this.wbs = wbs;
		this.report = report;
	}

	public EventRequestScreen(String adminNick, String playerNick, boolean wbs) {
		this(adminNick, playerNick, wbs, false);
	}

	public EventRequestScreen(String adminNick, String playerNick) {
		this(adminNick, playerNick, false, false);
	}

	public String getAdminNick() {
		return adminNick;
	}

	public String getPlayerNick() {
		return playerNick;
	}

	public boolean isWbs() {
		return wbs;
	}

	public boolean isReport() {
		return report;
	}
}
