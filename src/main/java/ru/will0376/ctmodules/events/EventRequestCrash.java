package ru.will0376.ctmodules.events;

import net.minecraftforge.fml.common.eventhandler.Event;

public class EventRequestCrash extends Event {
	private final String adminNick;
	private final String playerNick;

	public EventRequestCrash(String adminNick, String playerNick) {
		this.adminNick = adminNick;
		this.playerNick = playerNick;
	}

	public String getAdminNick() {
		return adminNick;
	}

	public String getPlayerNick() {
		return playerNick;
	}
}
