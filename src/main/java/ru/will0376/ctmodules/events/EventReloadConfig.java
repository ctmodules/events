package ru.will0376.ctmodules.events;

import net.minecraftforge.fml.common.eventhandler.Event;

public class EventReloadConfig extends Event {
	private final String sender;

	public EventReloadConfig(String sender) {
		this.sender = sender;
	}

	public String getSender() {
		return sender;
	}
}
