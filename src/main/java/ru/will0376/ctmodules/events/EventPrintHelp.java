package ru.will0376.ctmodules.events;

import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.eventhandler.Event;
import ru.will0376.ctmodules.events.utils.Logger;
import ru.will0376.ctmodules.events.utils.Utils;

public class EventPrintHelp extends Event {
	private final String playerMP;

	public EventPrintHelp(String player) {
		playerMP = player;
	}

	public String getPlayerNick() {
		return playerMP;
	}

	public void printToPlayer(String text) {
		if (playerMP.equals("Server")) {
			Logger.log(1, "", text);
			return;
		}
		if (Utils.getEPMP(playerMP) == null) {
			Logger.log(1, "[EPrintHelp]", "Wtf, player == null!");
			return;
		}
		Utils.getEPMP(playerMP).sendMessage(new TextComponentString(text));
	}
}
